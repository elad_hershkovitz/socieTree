import React, { Component } from 'react';
import './App.css';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Home from './components/Home';
import About from './components/About';
import Navbar from './components/navbar';


class App extends Component {
  render() {
    return (
      <div className="App">
      <Router>
        <div>
          <Navbar/>
          <Switch>
            <Route exact path='/' component={Home} />
          </Switch>
        </div>
        </Router>
      </div>
    );
  }
}

export default App;
