import React, { Component } from 'react';
import {Jumbotron, Grid, Row, Col, Image, Button, Carousel} from 'react-bootstrap';

export default class Home extends Component{
	render(){
		return(
    	   <div id="carouselExampleIndicators" className="carousel slide" data-ride="carousel">
                    <ol className="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" className="active" />
                        <li data-target="#carouselExampleIndicators" data-slide-to="1" />
                        <li data-target="#carouselExampleIndicators" data-slide-to="2" />
                    </ol>
                    <div className="carousel-inner">
                        <div className="carousel-item active">
                            <p><a href="/Trees"><img className="d-block w-100" src={require('../background.jpg')} alt="First slide" /></a></p>
                            <div className="carousel-caption d-none d-md-block">
                                <h3>Trees</h3>
                                <p>Browse through all the plants that are supported in your area.</p>
                            </div>
                        </div>
                        <div className="carousel-item">
                            <p><a href="/Locations"><img className="d-block w-100" src={require('../events.jpg')} alt="Second slide" /></a></p>
                            <div className="carousel-caption d-none d-md-block">
                                <h3>Stores and Locations</h3>
                                <p>All the beautiful parks where you can go.</p>
                            </div>
                        </div>
                        <div className="carousel-item">
                            <p><a href="/Stores"><img className="d-block w-100" src={require('../store.jpg')} alt="Third slide" /></a></p>
                            <div className="carousel-caption d-none d-md-block">
                                <h3>Events</h3>
                                <p>Find a store near you.</p>
                            </div>
                        </div>
                    </div>
                    {/*Left and Right arrows*/}
                    <a className="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span className="carousel-control-prev-icon" aria-hidden="true" />
                        <span className="sr-only">Previous</span>
                    </a>
                    <a className="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span className="carousel-control-next-icon" aria-hidden="true" />
                        <span className="sr-only">Next</span>
                    </a>
          </div> 
		)	
	}
}