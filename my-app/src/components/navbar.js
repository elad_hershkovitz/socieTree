import React, { Component } from 'react';
import { NavLink } from 'react-router-dom'


class Navbar extends Component {

	constructor (props) {
			super(props);
			this.state = {
				currentPage:""
			};
	};

	// Handle clicks on the navbar
	handleClick(event) {
			this.setState({
				currentPage: event.target.id
			});
		}

	render() {
		const pathname = window.location.href
		return (
			<div id="navbar">
				<h1 className="site-heading text-center text-back d-none d-lg-block">
					<span className="site-heading-lower">SOCIETREE</span>
				</h1>
				{/* Navigation */}

				<nav className="navbar navbar-expand-lg navbar-dark fixed-top bg-dark">
			      <a className="navbar-brand " href="#">Welcome to socieTree</a>
			      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			        <span className="navbar-toggler-icon"></span>
			      </button>

			      <div className="collapse navbar-collapse" id="navbarSupportedContent">
			        <ul className="navbar-nav mr-auto">
			          <li className="nav-item active">
			            <a className="nav-link" href="frontpage.html">Home <span className="sr-only">(current)</span></a>
			          </li>
			          <li className="nav-item dropdown">
			        <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			          Events
			        </a>
			        <div className="dropdown-menu" aria-labelledby="navbarDropdown">
			          <a className="dropdown-item" href="EventsPages/Events.html">Create Event</a>
			          <a className="dropdown-item" href="EventsPages/Events.html">Find Events</a>
			          <div className="dropdown-divider"></div>
			        </div>
			      </li>
			          <li className="nav-item">
			            <a className="nav-link" href="TreesPages\Trees.html">Trees</a>
			          </li>
			          <li className="nav-item">
			            <a className="nav-link" href="Stores\Stores.html">Stores</a>
			          </li>
			          <li className="nav-item">
			              <a className="nav-link" href="Locations\locations.html">Parks</a>
			          </li>
			          <li className="nav-item">
			            <a className="nav-link" href="aboutpage.html">About</a>
			          </li>
			        </ul>
			        <form className="form-inline my-2 my-lg-0">
			          <input className="form-control mr-sm-2" type="search" placeholder="Tree Search" aria-label="Search" />
			          <button className="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
			        </form>
			      </div>
			    </nav>
			</div>
		);
	}
}

export default Navbar;