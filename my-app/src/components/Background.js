import React, { Component } from 'react';

export default class Background extends React.Component {
	constructor() {
		super();
		this.state = {
			parks: [],
		};
	}

	componentDidMount() {
		fetch('https://data.austintexas.gov/resource/738i-aneg.json')
		.then(results => {
			return results.json();
		}).then(data => {
			let parks = data.results.map((parks) => {
					return (
						<div key = {parks.name}>
						<p>{parks.name}</p>
						</div>
					)
				})
			this.setState({parks: parks});
			console.log("state", this.state.parks);
			}
		)
	}

	render() {
		return(
			<div className="container2">
			<div className="container1">
				{this.state.parks}
			</div>
			</div>
		)
	}
}