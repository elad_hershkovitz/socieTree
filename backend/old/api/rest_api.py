#!/usr/bin/env python

from flask import Flask,jsonify,request
from flask_restless import APIManager
from flask_sqlalchemy import SQLAlchemy

# Create the Flask application and the Flask-SQLAlchemy object.
app = Flask(__name__)
app.config['DEBUG'] = True
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://societree:group6cs373@societreedb.cixb5yifossp.us-west-2.rds.amazonaws.com/societreedb'
db = SQLAlchemy(app)

class Trees(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Unicode)
    scientific_name = db.Column(db.Unicode, unique=True)
    plant_type = db.Column(db.Unicode)

class Parks(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    address = db.Column(db.Unicode)
    rating = db.Column(db.Unicode)
    name = db.Column(db.Unicode)

class Stores(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    address = db.Column(db.Unicode)
    rating = db.Column(db.Unicode)
    name = db.Column(db.Unicode)

class Events(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    url = db.Column(db.Unicode)
    start = db.Column(db.DateTime)
    end = db.Column(db.DateTime)
    name = db.Column(db.Unicode)


# Create the Flask-Restless API manager.
manager = APIManager(app, flask_sqlalchemy_db=db)

# Create API endpoints, which will be available at /api/<tablename> by
# default. Allowed HTTP methods can be specified as well.
manager.create_api(Trees, methods=['GET'])
manager.create_api(Parks, methods=['GET'])
manager.create_api(Stores, methods=['GET'])
manager.create_api(Events, methods=['GET'])

# start the flask loop
app.run(debug=True, host='0.0.0.0')
