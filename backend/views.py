from app import *
from flask_whooshee import Whooshee, AbstractWhoosheer
import whoosh.fields

whooshee = Whooshee(app)

@whooshee.register_model('name','scientific_name','tolerances','attributes','features','problems')
class Trees(db.Model):
    #__searchable__ = ['name','scientific_name','tolerances','attributes','features','problems']
    tree_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Unicode)
    scientific_name = db.Column(db.Unicode, unique=True)
    plant_type = db.Column(db.Unicode)
    image = db.Column(db.Unicode)
    leaf_type = db.Column(db.Unicode)
    growth_rate = db.Column(db.Unicode)
    water_needs = db.Column(db.Unicode)
    tolerances = db.Column(db.Unicode)
    attributes = db.Column(db.Unicode)
    features = db.Column(db.Unicode)
    comments = db.Column(db.Unicode)
    problems = db.Column(db.Unicode)
    store_id = db.Column(db.Unicode(500), db.ForeignKey('stores.store_id'))
    park_id = db.Column(db.Unicode(500), db.ForeignKey('parks.park_id'))
    event_id = db.Column(db.Unicode(500), db.ForeignKey('events.event_id'))


@whooshee.register_model('name','description')
class Events(db.Model):
    event_id = db.Column(db.Integer, primary_key=True)
    url = db.Column(db.Unicode)
    start = db.Column(db.DateTime)
    end = db.Column(db.DateTime)
    name = db.Column(db.Unicode)
    description  = db.Column(db.Unicode)
    rsvp_count = db.Column(db.Integer)
    trees = db.relationship('Trees', backref='events')

@whooshee.register_model('name','address')
class Parks(db.Model):    
    park_id = db.Column(db.Integer, primary_key=True)
    address = db.Column(db.Unicode)
    rating = db.Column(db.Unicode)
    name = db.Column(db.Unicode)
    photo_reference = db.Column(db.Unicode)
    trees = db.relationship('Trees', backref='parks')

@whooshee.register_model('name','address')
class Stores(db.Model):
    store_id = db.Column(db.Integer, primary_key=True)
    address = db.Column(db.Unicode)
    rating = db.Column(db.Unicode)
    name = db.Column(db.Unicode)
    hours = db.Column(db.Unicode)
    photo_reference = db.Column(db.Unicode)
    trees = db.relationship('Trees', backref='stores')

@app.route('/api/search')
def search():
    search = request.args.get('query')
    trees = Trees.query.whooshee_search(search).all()
    tree_response = {"trees":[{"tree_id":(d.tree_id),"name":(d.name),"scientific_name":(d.scientific_name),"growth_rate":(d.growth_rate),"tolerances":(d.tolerances),"attributes":(d.attributes),"features":(d.features),"problems":(d.problems)} for d in trees]}
    events = Events.query.whooshee_search(search).all()
    event_response = {"events":[{"event_id":(e.event_id),"name":(e.name),"description":(e.description)} for e in events]}
    parks = Parks.query.whooshee_search(search).all()
    park_response = {"parks":[{"park_id":(p.park_id),"name":(p.name),"address":(p.address)} for p in parks]}
    stores = Stores.query.whooshee_search(search).all()
    store_response = {"stores":[{"store_id":(s.store_id),"name":(s.name),"address":(s.address)} for s in stores]}
    return jsonify({**tree_response, **event_response, **park_response, **store_response})

@app.route('/api/trees/search')
def search_trees():
   search = request.args.get('query')
   trees = Trees.query.whooshee_search(search).all()
   tree_response = {"trees":[{"tree_id":(d.tree_id),"name":(d.name),"scientific_name":(d.scientific_name),"growth_rate":(d.growth_rate),"tolerances":(d.tolerances),"attributes":(d.attributes),"features":(d.features),"problems":(d.problems)} for d in trees]}
   return jsonify({**tree_response})

@app.route('/api/events/search')
def search_events():
    search = request.args.get('query')
    events = Events.query.whooshee_search(search).all()
    event_response = {"events":[{"event_id":(e.event_id),"name":(e.name),"description":(e.description)} for e in events]}
    return jsonify(event_response)  

@app.route('/api/parks/search')
def search_parks():
    search = request.args.get('query')
    parks = Parks.query.whooshee_search(search).all()
    park_response = {"parks":[{"park_id":(p.park_id),"name":(p.name),"address":(p.address)} for p in parks]}
    return jsonify(park_response)

@app.route('/api/stores/search')
def search_stores():
    search = request.args.get('query')
    stores = Stores.query.whooshee_search(search).all()
    store_response = {"stores":[{"store_id":(s.store_id),"name":(s.name),"address":(s.address)} for s in stores]}
    return jsonify(store_response)


@whooshee.register_whoosheer
class ModelWhoosheer(AbstractWhoosheer):
    # create schema, the unique attribute must be in form of
    # model.__name__.lower() + '_' + 'id' (name of model primary key)
    schema = whoosh.fields.Schema(
        tree_id = whoosh.fields.NUMERIC(stored=True, unique=True),
        event_id = whoosh.fields.NUMERIC(stored=True, unique=True),
        park_id = whoosh.fields.NUMERIC(stored=True, unique=True),
        store_id = whoosh.fields.NUMERIC(stored=True, unique=True),
        tree_name = whoosh.fields.TEXT(),
        event_name= whoosh.fields.TEXT(),
        park_name = whoosh.fields.TEXT(),
        store_name= whoosh.fields.TEXT(),
        park_address = whoosh.fields.TEXT(),
        store_address= whoosh.fields.TEXT(),
        description = whoosh.fields.TEXT(),
        scientific_name = whoosh.fields.TEXT(),
        tolerances = whoosh.fields.TEXT(),
        attributes = whoosh.fields.TEXT(),
        features = whoosh.fields.TEXT(),
        problems = whoosh.fields.TEXT())

    # don't forget to list the included models
    models = [Trees,Events,Parks,Stores]

    # create insert_* and update_* methods for all models
    # if you have camel case names like FooBar,
    # just lowercase them: insert_foobar, update_foobar

    @classmethod
    def update_trees(cls, writer, trees):
        writer.update_document(tree_id=trees.tree_id,
                               tree_name=trees.name,
                               scientific_name=trees.scientific_name,
                               tolerances=trees.tolerances,
                               attributes=trees.attributes,
                               features=trees.features,
                               problems=trees.problems
                               )
    
    @classmethod
    def update_events(cls, writer, events):
        writer.update_document(event_id=events.event_id,
                               event_name=events.name,
                               description=events.description
                               )
    @classmethod
    def update_parks(cls, writer, parks):
        writer.update_document(park_id=parks.park_id,
                               park_name=parks.name,
                               park_address=parks.address
                               )

    @classmethod
    def update_stores(cls, writer, stores):
        writer.update_document(store_id=stores.store_id,
                               store_name=stores.name,
                               store_address=stores.address
                               )
    @classmethod
    def insert_trees(cls, writer, trees):
        writer.add_document(tree_id=trees.tree_id,
                                   name=trees.name,
                                   scientific_name=trees.scientific_name,
                                   tolerances=trees.tolerances,
                                   attributes=trees.attributes,
                                   features=trees.features,
                                   problems=trees.problems
                                   )
    @classmethod
    def insert_events(cls, writer, events):
        writer.add_document(event_id=events.event_id,
                               event_name=events.name,
                               description=events.description
                               )
    @classmethod
    def insert_parks(cls, writer, parks):
        writer.add_document(park_id=parks.park_id,
                               park_name=parks.name,
                               park_address=parks.address
                               )

    @classmethod
    def insert_stores(cls, writer, stores):
        writer.add_document(store_id=stores.store_id,
                               store_name=stores.name,
                               store_address=stores.address
                               )

    @classmethod
    def delete_trees(cls, writer, trees):
        writer.delete_by_term('tree_id', trees.tree_id)

    @classmethod
    def delete_events(cls, writer, events):
        writer.delete_by_term('event_id', events.event_id,)


    @classmethod
    def delete_parks(cls, writer, parks):
        writer.delete_by_term('park_id', parks.park_id)

    @classmethod
    def delete_stores(cls, writer, stores):
        writer.delete_by_term('stores_id', stores.stores_id)

whooshee.reindex()

#Create the Flask-Restless API manager.
manager = APIManager(app, flask_sqlalchemy_db=db)

# Create API endpoints, which will be available at /api/<tablename> by
# default. Allowed HTTP methods can be specified as well
manager.create_api(Trees,methods=['GET'],results_per_page=50)
manager.create_api(Parks, methods=['GET'],results_per_page=50)
manager.create_api(Stores, methods=['GET'], results_per_page=50)
manager.create_api(Events, methods=['GET'],results_per_page=50)

app.run(debug='True', host='0.0.0.0')