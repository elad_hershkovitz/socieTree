from flask import Flask, jsonify, request, render_template
from flask_restless import APIManager
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS
from views import *

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://societree:group6cs373@societreedb.cixb5yifossp.us-west-2.rds.amazonaws.com/societreedb'

db = SQLAlchemy(app)
CORS(app)
