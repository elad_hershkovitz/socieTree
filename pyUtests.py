from mydatabase import app
from flask import Flask
from flask import url_for
import unittest
#from flask.ext.mysqldb import MySQL
from flask.mysql import MySQL



class Flask_Tests(unittest.TestCase):

    def test_basic_test(self):
        # Test unit tests are properly functioning
        result = 2 + 2
        self.assertEqual(result, 4)
                                            
    def test_config_user(self):
        # Test whether sql database user is properly configured
        result = app.config['MYSQL_USER']
        self.assertEqual(result,  'societree')

    def test_config_pw(self):
        # Test whether sql database password is properly configured
        result = app.config['MYSQL_PASSWORD']
        self.assertEqual(result, 'group6cs373')

    def test_config_database(self):
        # Test whether sql database is properly configured
        result = app.config['MYSQL_DB']
        self.assertEqual(result, 'societreedb')

    def test_config_host(self):
        # Test whether sql database host is properly configured
        result = app.config['MYSQL_HOST']
        self.assertEqual(result, 'societreedb.cixb5yifossp.us-west-2.rds.amazonaws.com')

if __name__ == "__main__":
    unittest.main()

