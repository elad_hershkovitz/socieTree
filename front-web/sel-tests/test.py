from sys import platform
import unittest
from unittest import TestCase
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

import requests

# options = webdriver.ChromeOptions()
# options.add_argument('--headless')

if platform == "linux" or platform == "linux2":
    driver = webdriver.Chrome('./chrome_drivers/chromedriver_linux')
elif platform == "darwin": # macOS
    driver=webdriver.Chrome('./chrome_drivers/chromedriver_macosx')
elif platform == "win32":
    driver=webdriver.Chrome('./chrome_drivers/chromedriver.exe')


driver.set_page_load_timeout(30)
driver.get("http://societree.me/#/")
driver.maximize_window()
driver.implicitly_wait(5)

class MyUnitTests (TestCase):
    def test1(self):
        """
        Iterate through items in navbar to see if clickable
        """
        links = driver.find_elements_by_tag_name("a")
        if not links:
            self.fail("No anchor tags found")
        for link in links:
            try:
                if(link.get_attribute('href')[0:5] == "https"):
                    continue
                req = requests.get(link.get_attribute('href'))
                if req.status_code >= 400:
                    self.fail("Bad link: " + str(link.get_attribute('href')))
            except Exception as e:
                print(e)
                self.fail("Something went wrong with testing to see if home page " \
                        "had broken links: " + str(link.get_attribute('href')))

    def test2(self):
        """
        Check that the components in the navbar exist
        """
        try:
            driver.find_element_by_link_text("Home").click()
            driver.find_element_by_link_text("Trees").click()
            driver.find_element_by_link_text("Stores").click()
            driver.find_element_by_link_text("Parks").click()
            driver.find_element_by_link_text("Stores").click()
            driver.find_element_by_link_text("About").click()
            pass
        except NoSuchElementException:
            self.fail("The element does not exist")

    def test3(self):
        """
        Check that trees has two clickable buttons at the top
        """
        driver.get("http://societree.me/#/trees")
        try:
            driver.find_element_by_link_text('Tools and accessories here!').click()
            driver.get("http://societree.me/#/trees")
            driver.find_element_by_link_text('How to plant your own tree').click()
            pass
        except NoSuchElementException:
            self.fail("The element is not clickable")


    @classmethod
    def tearDownClass(cls):
        driver.close()
        driver.quit()


if __name__ == "__main__":
    unittest.main()