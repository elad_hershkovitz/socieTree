import React from 'react'
import expect from 'expect'
import { shallow, mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-15'
import Parks from './components/parks.js'

describe('Component Parks', function() {
 it ('should have a class named parks', function() {
 const wrapper = Adapter.shallow(<Parks />);
 expect(wrapper.is('.parks')).to.equal(true);
 })
})
