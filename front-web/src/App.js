import React, { Component } from 'react';
import './App.css';
import Main from './components/Main';
class App extends Component {
  componentWillMount() {
    document.title = 'Societree';
  }

  render() {
    return (
      <Main/>
    );
  }
}

export default App;
