import React from 'react'
import expect from 'expect'
import { shallow, mount } from 'enzyme'
import {Adapter, configure} from 'enzyme-adapter-react-16'
import Trees from './components/trees.js'

configure({ adapter: new Adapter() })
describe('Component Trees', function() {
 it ('should have a class named trees', function() {
 const wrapper = shallow(<Trees />);
 expect(wrapper.is('.trees')).to.equal(true);
 })
})
