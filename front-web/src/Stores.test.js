import React from 'react'
import expect from 'expect'
import { shallow, mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-15'
import Stores from './components/stores.js'

describe('Component Stores', function() {
 it ('should have a class named stores', function() {
 const wrapper = Adapter.shallow(<Stores />);
 expect(wrapper.is('.stores')).to.equal(true);
 })
})
