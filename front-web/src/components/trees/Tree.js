import React from 'react';
import './Tree.css'
import {Link}from 'react-router-dom'
import {Thumbnail , Button}from 'react-bootstrap'
export default class Tree extends React.Component{
  
state = {
    tree:[]
  

}

componentDidMount() {
  fetch("http://api.societree.me/api/trees/"+this.props.match.params.id)
  .then(results => {
    return results.json();
  }).then(data=> this.setState({
    tree:data
    
  }));
}


render(){
   
return ( <div className ="item_tree">
<Thumbnail  src={this.state.tree.image} alt="242x200">
         <div id = 'treeItem'>
         <h3 >{this.state.tree.name} </h3>
          <p>Type: {this.state.tree.plant_type} </p>
          <p>scientific name: {this.state.tree.scientific_name} </p>
          <p>water needs: {this.state.tree.water_needs}</p>
          <p>problems: {this.state.tree.problems}</p>
          <p>leaf type: {this.state.tree.leaf_type}</p>
          <p>growth rate: {this.state.tree.growth_rate}</p>
          <p>attributes: {this.state.tree.attributes}</p>
          <p>features: {this.state.tree.features}</p>
          <p>comments: {this.state.tree.comments}</p>
          <Button id="button" bsStyle="primary" href="https://www.starkbros.com/products/tools-and-supplies/tree-accessories">Tools and accessories here!</Button>&nbsp;
          <Button id="button" bsStyle="primary" href="https://www.bhg.com/gardening/trees-shrubs-vines/care/the-proper-way-to-plant-a-tree/">How to plant your own tree</Button>&nbsp;
          <Link to ="/trees">
          <Button id="button" bsStyle="primary">GO BACK</Button>&nbsp;
         </Link>
         </div>
         </Thumbnail> 
         
      </div>        
);
}
}
