import React, { Component } from 'react';
import {Router, Route, Link, RouteHandler, Redirect} from 'react-router-dom';
import {Select} from 'react-select';
import 'react-select/dist/react-select.css';
import Pagination from '../pagination';
import {Thumbnail,Col,Button} from 'react-bootstrap';
import Search from './treeSearch';
class Locations extends Component {

	constructor () {
		super();
		this.state = {
			trees:[],
			locations: [],
			pageOfItems: [],
			navigate: false,
			navigateTo: '',
			selectedLocation: [],
			navigateTo: "",
			trees_list: [],
			
			selectedSize: {
				value: undefined,
				label: undefined
			},
      selectedGrowthRate: {
				value: undefined,
				label: undefined
			},
			selectedSort: {
				value: undefined,
				label: undefined,
			},
			sort_by: undefined,
			sort_attr: undefined
		};
		this.returnNoResults = this.returnNoResults.bind(this);
		this.onChangePage = this.onChangePage.bind(this);
	};


	fetchTree=()=>{
	const min = 1;
    const max = 33;
    let rand = Math.round(min + Math.random() * (max - min));
	
	return(rand);
	}
	
	// Initial load of data into page
	componentDidMount() {
		fetch("http://api.societree.me/api/trees")
		.then(results => {
		  return results.json();
		}).then(data=> this.setState({
		  trees:data.objects,
		}))
		this.getTrees();
	  }

	  onChangePage(pageOfItems) {
		// update state with new page of items
    
		this.setState({ pageOfItems: pageOfItems });
	};	  

	// Renders data after fetch call
	

	// Gets the list of possible trees for the tree filter
	getTrees() {
		fetch("http://api.societree.me/api/trees").then(results =>{
			return results.json();
		}).then(data=>{
			let trees = data.objects.map((tree) =>{
				return(
					{value: tree.name, label: tree.name}
				)
			})
			this.setState({trees_list: trees});
		})
	};

	// Sets state to selected tree from filter
	handleTreeChange(selectedTree){
		if (selectedTree == null) {
			this.state.selectedTree = {
				value: undefined,
				label: undefined
			};
		} else if (selectedTree){
			this.state.selectedTree = selectedTree;
			this.setState({selectedTree: selectedTree});
		}
		this.update();
	}
	handleGrowthChange(selectedGrowthRate){
		if (selectedGrowthRate == null) {
			this.state.selectedGrowthRate = {
				value: undefined,
				label: undefined
			};
		} else if (selectedGrowthRate){
			this.state.selectedGrowthRate = selectedGrowthRate;
			this.setState({selectedGrowthRate: selectedGrowthRate});
		}
		this.update();
	}

	// Sets state to selected method to sort by
	handleSortChange(selectedSort) {
		if (selectedSort == null) {
			this.state.selectedSort = {
				value: undefined,
				label: undefined
		}
		} else if (selectedSort){
		
			this.state.selectedSort = selectedSort;
			this.setState({ selectedSort:selectedSort });
		}
		this.update();
	}

	// Set state to the selected size cutoff
	handleSizeChange (selectedSize){

		if (selectedSize == null) {
			this.state.selectedSize = {
				value: undefined,
				label: undefined
			};
		} else if (selectedSize) {
			this.state.selectedSize = selectedSize;
			this.setState({selectedSize: selectedSize});
		}
		this.update();
	}

	// If no data is return from fetch call, print No Results message
	returnNoResults() {
			return (
				<div className="intro-text text-center bg-faded p-5 rounded">
						<span className="section-heading-upper text-center">No Results</span>
				</div>
			)
		}

	update () {
	
		
    let sortfilter = this.state.selectedSort.value;
		let sizeFilter = this.state.selectedSize.value;
    let growthFilter= this.state.selectedGrowthRate.value;
    let sortUrl='"order_by":[{"field":"name","direction":"'+sortfilter+'"}]';
    let sizeUrl ='{"name":"plant_type","op":"==","val":"'+sizeFilter+'"}';
    let growthUrl ='{"name":"growth_rate","op":"==","val":"'+growthFilter+'"}';
    let url =sizeUrl;

    if(sizeFilter&&growthFilter) { url =sizeUrl+','+growthUrl;}
     else if (growthFilter){url=growthUrl;}
		 url='"filters":['+url+']';

		if(sortfilter&&growthFilter||sortfilter&&sizeFilter){url+=','+sortUrl;}
		else if(sortfilter) url = sortUrl;

		if(sortfilter||sizeFilter||growthFilter)
		
		
		
		{	
			fetch('http://api.societree.me/api/trees?q={'+url+'}')
      
			.then(results => {
        
			  return results.json();
			})
      
      .then(data=> this.setState({
  
          
			
          
        trees:data.objects
          
        
			}))
      }
      
		else this.componentDidMount();
    
		  }
     
	render() {
   
	const	 tree=
              
     this.state.pageOfItems.map((item,index) =>
                      <Link to ={'/trees/'+item.tree_id} key ={item.tree_id} >
                      <Col xs={6} md={4}>
                        <Thumbnail rounded className = 'tree' src={item.image} alt="loading">
                          <h5 id='tree'>{item.name}</h5>
                         
                         </Thumbnail>
                      </Col>
                      </Link>
                    );
        
   
  

		if (this.state.navigate) {
			return <Redirect to={{pathname: this.state.navigateTo, state: {selectedLocation: this.state.selectedLocation}}} push={true} />;
		}

		

		
		const SelectPackage = require('react-select');
		const Select = SelectPackage.default;
		const {selectedTree} = this.state;
		const {selectedSize} = this.state;
    const {selectedGrowthRate} = this.state;
		const {selectedSort} = this.state;
    

		const treeValue = selectedTree && selectedTree.value;
		const sizeValue = selectedSize && selectedSize.value;
    const growthValue = selectedGrowthRate && selectedGrowthRate.value;
		const sortValue = selectedSort && selectedSort.value

		return (



	<div>
		<h1 className= "text-center "style={{color:'green'}}> Trees </h1>

		<div className="filters-and-grid">
			<h3>
			<Search url='http://api.societree.me/api/trees/search?query='/>
			<div className='text-center'>
			<Link to ={'/trees/'+this.fetchTree()}>
			<Button  id="button" bsStyle="danger"><h3>Fetch random Tree</h3></Button>
			</Link>
			</div> 
			</h3>
			<div className="row text-center">
						
						<div className="filter col-4">
							<h3 style={{color:"white"}}>Filter size
							<Select
									id="sizefilter"
									name="form-field-name"
									value={sizeValue}
									onChange={this.handleSizeChange.bind(this)}
									options={[
										{value: 'Small', label: 'Small'},
										{value: 'Medium', label: 'Medium'},
										{value: 'Large', label: 'Large'}
										
									]}
                
							/>
              </h3>
              </div>
              <div className="filter col-4">
                  <h3 style={{color:"white"}}>Filter growth rate
              <Select
									id="growthfilter"
									name="form-field-name"
                  value={growthValue}
									onChange={this.handleGrowthChange.bind(this)}
									options={[
										{value: 'Rapid', label: 'Rapid'},
										{value: 'Moderate', label: 'Moderate'},
										{value: 'Slow', label: 'Slow'}
										
									]}
							/>
						</h3>
						</div>
						<div className="filter col-4">
							<h3 style={{color:"white"}}>Sort By
							<Select
									id="sort"
									name="form-field-name"
									value={sortValue}
									onChange={this.handleSortChange.bind(this)}
									options={[
										{value: 'asc', label: 'Name: A - Z'},
										{value: 'desc', label: 'Name: Z - A'},
									]}
							/>
							</h3>
						</div>
					</div>
					{tree}
					</div>
				
					<Pagination items={this.state.trees} onChangePage={this.onChangePage.bind(this)} />
				</div>
			);
	}
}

export default Locations;