
import React from 'react';
import {Link} from 'react-router-dom';
import Highlighter from "react-highlight-words";


class Search extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pageOfItems:[ ],
      trees: [ ],
      term: '',
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

   handleChange(event) {
    this.setState({value: event.target.value});
    
  }

  handleSubmit(event) {
   let termSearch = this.props.searchTerm;
   let url = this.props.url+ encodeURI(this.state.value) + '&json=1';
     fetch(url)
   .then(results => {
     return results.json();
     }).then(
      data=> this.setState({
      trees:data.trees,
      results:(data.trees.length===0)?
      <h1 className='text-center' style={{color:'red'}}> OOPS.. No results were found</h1>:
      <h1 className='text-center' style={{color:'white'}} >found {data.trees.length} results</h1>
       }));
      
 }

  onChangePage(pageOfItems) {
		// update state with new page of items
    
		this.setState({ pageOfItems: pageOfItems });
	};	  
  highlightText(text) {
		return (
			<Highlighter
				 highlightClassName={ {color:'black'}}
				 highlightStyle={{ color: 'black' }}
				 searchWords = {this.state.value.split(" ")}
				 autoEscape={false}
				 textToHighlight= {text}
			/>
		)
}


  render() {

                const	 tree  = 
                
              this.state.trees.map((item,index) =>
                                
                                <Link to ={'/trees/'+item.tree_id} key ={item.tree_id} >
                                  <li xs={6} md={4}>
                                    
                                      <h5 style={{color:'white'}}>{this.highlightText(item.name)}</h5>
                                  
                                        
                                  {(item.scientific_name).toUpperCase().includes(this.state.value.toUpperCase())&&<h5 > scientific name:  {this.highlightText(item.scientific_name)}</h5>}      
                                {(item.attributes).toUpperCase().includes(this.state.value.toUpperCase())&&<h5>attributes : {this.highlightText(item.attributes)}</h5>}
                                {(item.features).toUpperCase().includes(this.state.value.toUpperCase())&&<h5>features :{this.highlightText(item.features)}</h5>}
                                  {(item.growth_rate).toUpperCase().includes(this.state.value.toUpperCase())&& <h5 > growth rate :{this.highlightText(item.growth_rate)}</h5>}   
                                  {(item.problems).toUpperCase().includes(this.state.value.toUpperCase())&&  <h5 > problems {this.highlightText(item.problems)}</h5>}
                                  </li>
                                  </Link>
                              );
    return (
      <div>
         <form  className='text-center' onSubmit={this.handleSubmit}>
        <label>
        <input  type="text" value={this.state.value} onChange={this.handleChange} />
        </label>
        <input type="submit" value="Search" />
      </form>
      {this.state.results}
  
      
      {this.state.trees.length > 0 
     && <h1  style={{color:'white'}} >Trees results</h1>
      			
      } {tree}    
       
    </div>
    
  );
  }
}

export default Search;

