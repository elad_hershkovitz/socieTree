import React, { Component } from 'react';
import { Collapse, Button } from 'reactstrap';

export default class CollapseInfo extends Component {
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.state = { collapse: false };
  }

  toggle() {
    this.setState({ collapse: !this.state.collapse });
  }

  render() {
    return (
      <div>
        <Button color="primary" onClick={this.toggle} style={{ marginBottom: '1rem' }}>info</Button>
        <Collapse isOpen={this.state.collapse}>
          
            
            {this.props.bio}
            
          
        </Collapse>
      </div>
    );
  }
}
