import React from 'react';
import {Jumbotron,Button} from 'react-bootstrap';
import './about.css';
import CollapseInfo from './hideInfo';
import {Card} from 'reactstrap';


export default class About extends React.Component{
    
 state ={
    
     developers :[
        {name : "Elad Hershkovitz",
         bio:
         <span>
        <h3>
         Elad Hershkovitz is in his third year in his major of computer science major.</h3>
         <h3>
         his favorit tree is Red maple.He was responsible of the front-end.
         </h3>
         </span>,
         src:"https://cdn-images-1.medium.com/max/1600/1*S0IuvaQ24Paf-1lxgFlngg.jpeg",
         commits : 0,
         issues : 0,
         unit_test : 0,
        isToggleOn:true, 

        },
        {name : "Miranda Fulenchek",
         bio:<span>
         <h3>
         Miranda Fulenchek is an upcoming senior this fall majoring in Computer Science</h3>
         <h3> and her favorite tree is the Live Oak.</h3>
         <h3> She was responsible for the backend.</h3>
          </span>,
         src: "https://cdn-images-1.medium.com/max/1600/1*WN7zejjs6U6pluEwBWbVLw.jpeg",
         commits : 0,
         issues : 0,
         unit_test : 0,
         linkedIn:"https://www.linkedin.com/in/miranda-fulenchek-12001970/",
        isToggleOn:true, 
    },
        {name : "Sharmin Anwar",bio:<h3>Sharmin is a math/cs major and her favorite tree is the dessert willow</h3>,src : "https://avatars3.githubusercontent.com/u/8905232?s=400&u=376e941c4bfb10a34e46500a5163ecf008807347&v=4",
        commits : 0,
        issues : 0,
        unit_test : 0},
        {name : "Isabella Robledo",bio:<span>
        <h3>Isabella Robledo is graduating this summer with a degree in CS </h3>
         <h3>    and her favorite tree is the Oak.</h3>
         </span>
        ,src:"https://cdn-images-1.medium.com/max/1600/1*MPgIQRgF7BoEymhMcdwMKw.png",
        commits : 0,
        issues : 0,
        unit_test : 0,
        linkedIn:"https://www.linkedin.com/in/isabella-robledo-04491091/",
        isToggleOn:true, },
        {name : "Sharanya Agarwal ",bio:<h3>Sharanya is a CS major in her  senior year ,her  favorite tree is Somei Yoshino tree</h3>,src: "https://sharanyaagarwal.files.wordpress.com/2018/06/screen-shot-2018-05-25-at-9-50-42-pm-e1528661433499.png",
        commits : 0,
        issues : 0,
        unit_test : 0,
        linkedIn:"https://www.linkedin.com/in/sharanya-agarwal/",
        isToggleOn:true}
     ],
     totalIssues :0,
     totalCommits:0,
     tools:[
        {name:'Amazon Web Services (AWS)',desc: 'Hosts Website, Frontend and backend',link:""},
        {name:"GitLab",desc: "Code Repository",link:"" },
       { name:"React-Bootstrap",desc: 'Used to Style CSS',link:""},
       {name:"mySQL",desc: "database",link:""},
       {name: "Postman",desc: "REST API Testing and Documentation" ,link:""},
       { name:"Namescheap",desc: "Domain Name",link:""},
        {name:"Google-Docs",desc: "For collaborative Editing of Tech Report",link:""},
        {name:"Whooshee",desc: "Used for Searching",link:""},
        {name:"Flask-Restless",desc: "Used to Create our API",link:""},
        {name:"React.JS",desc: "JavaScript front end design",link:""},
        {name:"React-select",desc: "to filter and sort on model pages",link:""},
        {name:"React-highlight-word",desc: "to highlight search words in the results",link:""},
        {name:"React-router",desc: "JavaScript front end design",link:""},
        {name: "Selenium", desc: "GUI too test the front-end", link:""},
        {name: "Jest", desc: "To test the frontend", link:""}
     ]
    }
   
   
    componentDidMount(){
    
        
   
        fetch('https://gitlab.com/api/v4/projects/7212025/repository/contributors?per_page=400&private_token=CXgrygkJydiSupGyoaQ7')
            .then(response => response.json())
            .then(data =>  {
                    for (var i of data){
                        this.state.totalCommits += i.commits;
                    if(i.name === 'Sharanya Agarwal') {
                        this.state.developers[4].commits += i.commits;
                    }
                    if(i.name === 'mfulenchek') {
                        this.state.developers[1].commits  += i.commits;
                    }
                    if(i.name ==="eladhershkovitz"||i.author_name ==='elad') {
                        this.state.developers[0].commits  += i.commits;
                    }
                    if(i.name === 'Isabella Robledo') {
                        this.state.developers[3].commits  += i.commits;
                    }
                    if(i.name === "sanwar2") {
                        this.state.developers[2].commits  += i.commits;
                    }
                }
                                
                this.setState(
                    {ready: true}
                );
            })
            .catch(err => console.error(this.props.url, err.toString()));
    
        fetch('https://gitlab.com/api/v4/projects/7212025/issues?private_token=CXgrygkJydiSupGyoaQ7&updated_after=2018-06-22T00:00:00.000Z-&per_page=400')
            .then(response => response.json())
            .then(data => {
                for(var i of data){
                    this.state.totalIssues++;
                    if(i.author.name == 'Sharanya Agarwal') {
                        this.state.developers[4].issues += 1;
                    }
                    if(i.author.name == 'Miranda Fulenchek') {
                        this.state.developers[1].issues  +=1;
                    }
                    if(i.author.name == "elad") {
                        this.state.developers[0].issues  +=1;
                    }
                    if(i.author.name == 'Isabella Robledo') {
                        this.state.developers[3].issues  +=1;
                    }
                    if(i.author.name == "sanwar2") {
                        this.state.developers[2].issues  += 1;
                    }
                     
                }            
                this.setState(
                    {ready: true}
                );
            })
            .catch(err => console.error(this.props.url, err.toString()));
    

    }







render(){
const devps= (
<div>{
this.state.developers.map((item,index) =>{
   
return (
    <div  className="dev"key={index}>
    <img className="rounded " style={{width:'300px',height:'200px'}} src={item.src}alt="image" />
    
      <h3>{item.name}</h3>
       <CollapseInfo key={index}  id={index} bio={item.bio}/>
    
     <h3>commits: {item.commits}</h3>
     <h3> issues: {item.issues}</h3>
      
    
  </div>
  
  
)})}
</div>
 );

const tools = (

this.state.tools.map((tool,index) =>{
   
return (
    
        <div >
          <Card>
          <h2>{tool.name}</h2>
          {tool.desc}
          </Card>
          </div>
           
      
        
)})
 );








    
    return (
        <div className="text-center">
            <div >
            <Jumbotron  id = 'jumbo'>
            <h1> Welcome to socieTree </h1>
        <p>
        We are Computer science students from UT Austin who love Trees!
        Our intention is to create a website to help people find trees as well and find related events. We hope to reach anyone with an interest in gardening and helping the environment. 
        </p>
        <h2> Total commits : {this.state.totalCommits}</h2>
        <h2> Total issues : {this.state.totalIssues}</h2>
        
        <p>
            <Button bsStyle="primary" href="https://gitlab.com/elad_hershkovitz/socieTree">GitLab</Button>
        </p>
        <p>
            <Button bsStyle="primary" href="http://d3practiceone.s3-website-us-west-2.amazonaws.com/">Visualizations</Button>
        </p>
        </Jumbotron>
        <h2 style={{color:'white',backgroundColor:"black"}}> Our team : </h2>    
        </div>
       <div>
        {devps}
        </div>
        <h1 style={{color:'white',backgroundColor:"black"}}>Tools used to create this Website</h1>
        {tools}
        </div>


    );
}


}
