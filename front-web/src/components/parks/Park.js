import React from 'react';
import './Park.css'
import {Link}from 'react-router-dom'
import {Thumbnail , Button}from 'react-bootstrap'
export default class parks extends React.Component{
  
state = {
    park:[]
  

}

componentDidMount() {
  fetch("http://api.societree.me/api/parks/"+this.props.match.params.id)
  .then(results => {
    return results.json();
  }).then(data=> this.setState({
    park:data
    
  }));
}


render(){
   
return ( <div className ="item_parks">
<Thumbnail src={"https://maps.googleapis.com/maps/api/place/photo?maxwidth=5000&photoreference="+this.state.park.photo_reference +"&key=AIzaSyAKiUXcyeqYZsgTieBEdrDjcr2qsHT06BU"} alt="242x200">
          
         <h3>{this.state.park.name} </h3>
          <h3>Address :{this.state.park.address} </h3>
          <h3>Rating:{this.state.park.rating} </h3>
          <Link to ="/parks">
          <Button>GO Back</Button>
         </Link>
         </Thumbnail> 
         </div>        
);
}
}
