
import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import Highlighter from "react-highlight-words";
class Search extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
         
      parks:[],
      results:[],
      term: '',
      
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

   handleChange(event) {
    this.setState({value: event.target.value});
  }

  handleSubmit(event) {
   

   let url = 'http://api.societree.me/api/parks/search?query='+ encodeURI(this.state.value) + '&json=1';
     fetch(url)
   .then(results => {
     return results.json();
     }).then(data=> this.setState({
      parks:data.parks,
      results:(data.parks.length===0)?
      <h1 className='text-center' style={{color:'red'}}> OOPS.. No results were found</h1>:
      <h1 className='text-center' style={{color:'white'}} >found {data.parks.length} results</h1>
     
       }));
 }

  onChangePage(pageOfItems) {
		// update state with new page of items
    
		this.setState({ pageOfItems: pageOfItems });
	};	  
  highlightText(text) {
		return (
			<Highlighter
				 highlightClassName={ {color:'black'}}
				 highlightStyle={{ color: 'black' }}
				 searchWords = {this.state.value.split(" ")}
				 autoEscape={false}
				 textToHighlight= {text}
			/>
		)
}

  render() {

  
    const	 parks  =        
    this.state.parks.map((item,index) =>
                      <Link to ={'/parks/'+item.park_id} key ={item.event_id} >
                       <ol start={index+1}xs={6} md={4}>
                          
                       <h3 style={{color:'white'}}> <li> {this.highlightText(item.name)}</li></h3>
                  {/*           
                              {(item.address).toUpperCase().includes(this.state.value.toUpperCase())&&<h3 > address:  {this.highlightText(item.address)}</h3>}      
                      {(item.rating).toUpperCase().includes(this.state.value.toUpperCase())&&<h3>end time : {this.highlightText(item.rating)}</h3>}
                    */} 
                     
   
                        </ol>
                        </Link>
                   );
 
 

    return (
      <div className='text-center'>
         <form  className='text-center' onSubmit={this.handleSubmit}>
        <label>
        <input  type="text" value={this.state.value} onChange={this.handleChange} />
        </label>
        <input type="submit" value="Search" />
      </form>

      {this.state.results}
  
      
   
    
            
   
  
     {parks}
   
    
    
      </div>
    
       
   
 
  
   );
 }
}
export default Search;

