
import React from 'react';

import {Link} from 'react-router-dom';

import Highlighter from "react-highlight-words";


class Search extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pageOfItems: [],
      trees: [],
      events:[],
      parks:[],
      stores:[],
      results:[]
    
      
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

   handleChange=(event) =>{
    this.setState({value: event.target.value});
  }

  handleSubmit=(event) =>{
    
   let url = 'http://api.societree.me/api/search?query='+ encodeURI(this.state.value) + '&json=1';
     fetch(url)
   .then(results => {
     return results.json();
     }).then(data=> this.setState({
      trees:data.trees,
      events:data.events,
      parks:data.parks,
      stores:data.stores,
      results:(data.trees.length+data.events.length+data.parks.length+data.stores.length===0)?
      <h1 className='text-center' style={{color:'red'}}> OOPS.. No results were found</h1>:
      <h1 className='text-center' style={{color:'white'}} >found {data.trees.length+data.events.length+data.parks.length+data.stores.length} results</h1>
      
       }));
       
    
 }

  onChangePage(pageOfItems) {
		// update state with new page of items
    
		this.setState({ pageOfItems: pageOfItems });
	};	  
  highlightText(text) {
		return (
			<Highlighter
				 highlightClassName={ {color:'black'}}
				 highlightStyle={{ color: 'black' }}
				 searchWords = {this.state.value.split(" ")}
				 autoEscape={false}
				 textToHighlight= {text}
			/>
		)
}
checkForResults =()=> {return ( this.state.trees.length ===0 && this.state.stores.length ===0 &&this.state.parks.length === 0 &&
 this.state.events.length === 0 );};

  render() {

    const	 tree  =        
 this.state.trees.map((item,index) =>
                  
                   <Link to ={'/trees/'+item.tree_id} key ={item.tree_id} >
                    <ol start={index+1} xs={6} md={4}>
                       
                        <h3 style={{color:'white'}}> <li> {this.highlightText(item.name)}</li></h3>
                    
                           
                    {(item.scientific_name).toUpperCase().includes(this.state.value.toUpperCase())&&<h3 >At Scientific name:  {this.highlightText(item.scientific_name)}</h3>}      
                   {(item.attributes).toUpperCase().includes(this.state.value.toUpperCase())&&<h3>At  Attributes : {this.highlightText(item.attributes)}</h3>}
                   {(item.features).toUpperCase().includes(this.state.value.toUpperCase())&&<h3> At Features :{this.highlightText(item.features)}</h3>}
                    {(item.growth_rate).toUpperCase().includes(this.state.value.toUpperCase())&& <h3 >At  Growth rate :{this.highlightText(item.growth_rate)}</h3>}   
                     {(item.problems).toUpperCase().includes(this.state.value.toUpperCase())&&  <h3 > At Problems :{this.highlightText(item.problems)}</h3>}
                     </ol>
                     </Link>
                );
const	 event  =        
 this.state.events.map((item,index) =>
                   <Link to ={'/events/'+item.event_id} key ={item.event_id} >
                    <ol start={index+1}xs={6} md={4}>
                       
                    <h3 style={{color:'white'}}> <li> {this.highlightText(item.name)}</li></h3>
                          {(item.description).toUpperCase().includes(this.state.value.toUpperCase())&&<h3 > description:  {this.highlightText(item.description)}</h3>}      
                 {/* {(item.end).toUpperCase().includes(this.state.value.toUpperCase())&&<h3>end time : {this.highlightText(item.end)}</h3>}
                   {(item.start).toUpperCase().includes(this.state.value.toUpperCase())&&<h3>start time :{this.highlightText(item.start)}</h3>}
                  */}

                     </ol>
                     </Link>
                );
const	 store  =        
 this.state.stores.map((item,index) =>
                   <Link to ={'/stores/'+item.store_id} key ={item.event_id} >
                    <ol start={index+1} xs={6} md={4}>
                       
                    <h3 style={{color:'white'}}> <li> {this.highlightText(item.name)}</li></h3>
                   {/*      
                             {(item.description).toUpperCase().includes(this.state.value.toUpperCase())&&<h3 > description:  {this.highlightText(item.description)}</h3>}      
                   {(item.end).toUpperCase().includes(this.state.value.toUpperCase())&&<h3>end time : {this.highlightText(item.end)}</h3>}
                   {(item.start).toUpperCase().includes(this.state.value.toUpperCase())&&<h3>start time :{this.highlightText(item.start)}</h3>}
                  */}

                     </ol>
                     </Link>
                );
const	 park  =        
 this.state.parks.map((item,index) =>
                   <Link to ={'/parks/'+item.park_id} key ={item.event_id} >
                    <ol start={index+1}xs={6} md={4}>
                       
                    <h3 style={{color:'white'}}> <li> {this.highlightText(item.name)}</li></h3>
               {/*           
                           {(item.address).toUpperCase().includes(this.state.value.toUpperCase())&&<h3 > address:  {this.highlightText(item.address)}</h3>}      
                   {(item.rating).toUpperCase().includes(this.state.value.toUpperCase())&&<h3>end time : {this.highlightText(item.rating)}</h3>}
                 */} 
                  

                     </ol>
                     </Link>
                );
 
 
  

    return (
      <div>
              <h3>
                    <form className='text-center'>
                    
                    <input  type="text" name='search' placeholder='search for..' value={this.state.search} onChange={this.handleChange.bind(this)} />
                    
                    <button className=" btn-success" type="button" onClick = {this.handleSubmit.bind(this)}> Search</button>
                  </form>
                  <h1>{this.state.results}</h1>
              </h3>
              <div className='text-center'>
              {this.state.trees.length+this.state.events.length+this.state.parks.length+this.state.stores.length===0&&<img style={{width:'50%',marginTop:"20px"}}src="https://images.unsplash.com/photo-1505028106030-e07ea1bd80c3?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=6780283deae869993510eb7902bd5f83&auto=format&fit=crop&w=500&q=60"/>}
              </div>    
                  {this.state.trees.length > 0 
                && <h1 className="text-center" style={{color:'white'}} > #Trees results : {this.state.trees.length}</h1>
                        
                  }
                   {tree}

                {this.state.events.length > 0 &&    
                <h1 className="text-center"  style={{color:'white'}} >#Events results : {this.state.events.length}</h1>	
                        
                }
                {event}

              {this.state.parks.length > 0 &&    
                <h1  className="text-center" style={{color:'white'}} >#Parks results : {this.state.parks.length}</h1>	
                        
                }{park}

                  {this.state.stores.length > 0 &&    
                <h1  className="text-center"style={{color:'white'}} >#Stores results : {this.state.stores.length}</h1>	
                        
                }{store}
    
       
    </div>
    
  );
  }
}

export default Search;

