
import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import Highlighter from "react-highlight-words";


class Search extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      events:[],
      term: '',
      results:[]
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

   handleChange(event) {
    this.setState({value: event.target.value});
  }

  handleSubmit(event) {
   

   let url = 'http://api.societree.me/api/events/search?query='+ encodeURI(this.state.value) + '&json=1';
     fetch(url)
   .then(results => {
     return results.json();
     }).then(data=> this.setState({
      
      events:data.events,
      results:(data.events===0)?
      <h1 className='text-center' style={{color:'red'}}> OOPS.. No results were found</h1>:
      <h1 className='text-center' style={{color:'white'}} >found {data.events.length} results</h1>
       }));
 }

  onChangePage(pageOfItems) {
		// update state with new page of items
    
		this.setState({ pageOfItems: pageOfItems });
	};	  
  highlightText(text) {
		return (
			<Highlighter
				 highlightClassName={ {color:'black'}}
				 highlightStyle={{ color: 'black' }}
				 searchWords = {this.state.value.split(" ")}
				 autoEscape={false}
				 textToHighlight= {text}
			/>
		)
}


  render() {

    
    const	 event  =        
    this.state.events.map((item,index) =>
                      <Link to ={'/events/'+item.event_id} key ={item.event_id} >
                       <ol start={index+1}xs={6} md={4}>
                          
                       <h3 style={{color:'white'}}> <li> {this.highlightText(item.name)}</li></h3>
                             {(item.description).toUpperCase().includes(this.state.value.toUpperCase())&&<h3 > description:  {this.highlightText(item.description)}</h3>}      
                    {/* {(item.end).toUpperCase().includes(this.state.value.toUpperCase())&&<h3>end time : {this.highlightText(item.end)}</h3>}
                      {(item.start).toUpperCase().includes(this.state.value.toUpperCase())&&<h3>start time :{this.highlightText(item.start)}</h3>}
                     */}
   
                        </ol>
                        </Link>
                   );
    return (
      <div>
         <form  className='text-center' onSubmit={this.handleSubmit}>
        <label>
        <input  type="text" value={this.state.value} onChange={this.handleChange} />
        </label>
        <input type="submit" value="Search" />
      </form>
      {this.state.results}
      {event}
    </div>
    
  );
  }
}

export default Search;

