import React from 'react'
import expect from 'expect'
import { shallow, mount } from 'enzyme'
import {Adapter, configure} from 'enzyme-adapter-react-16'
import Events from './components/events.js'

configure({ adapter: new Adapter() })

describe('Component Events', function() {
 it ('should have a class named events', function() {
 const wrapper = shallow(<Events />);
 expect(wrapper.is('events')).toEqual(true);
 })
})
