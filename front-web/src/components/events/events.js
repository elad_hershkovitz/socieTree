import React, { Component } from 'react';
import {Link, Redirect} from 'react-router-dom';
import Search from './eventSearch'
import 'react-select/dist/react-select.css';
import Pagination from '../pagination';
import {Thumbnail,Col} from 'react-bootstrap';
class Locations extends Component {

	constructor () {
		super();
		this.state = {
			events:[],
			pageOfItems: [],
			events_list: [],
			selectedRvsp: {
				value: undefined,
				label: undefined
			},
      
			selectedSort: {
				value: undefined,
				label: undefined,
			},
			
		};
		
		this.onChangePage = this.onChangePage.bind(this);
	};

	// Initial load of data into page
	componentDidMount() {
		fetch("http://api.societree.me/api/events")
		.then(results => {
		  return results.json();
		}).then(data=> this.setState({
		  events:data.objects,
		}))
		
	  }

	  onChangePage(pageOfItems) {
		// update state with new page of items
    
		this.setState({ pageOfItems: pageOfItems });
	};	  

	
	handleRvspChange(selectedRvsp){
		if (selectedRvsp == null) {
			this.state.selectedRvsp = {
				value: undefined,
				label: undefined
			};
		} else if (selectedRvsp){
			this.state.selectedRvsp = selectedRvsp;
			this.setState({selectedRvsp: selectedRvsp});
		}
		this.update();
	}

	// Sets state to selected method to sort by
	handleSortChange(selectedSort) {
		if (selectedSort == null) {
			this.state.selectedSort = {
				value: undefined,
				label: undefined
		}
		} else if (selectedSort){
		
			this.state.selectedSort = selectedSort;
			this.setState({ selectedSort:selectedSort });
		}
		this.update();
	}

	
	update () {
	
		
	let sort_by = this.state.selectedSort.value;
   	let rvspFilter= this.state.selectedRvsp.value;
    let rvspUrl ='"filters":[{"name":"rsvp_count","op":"ge","val":"'+rvspFilter+'"}]';
    let sortUrl='"order_by":[{"field":"name","direction":"'+sort_by+'"}]';
    let url = rvspUrl;
   if(sort_by && rvspFilter){
       url =rvspUrl+','+sortUrl;
   }
   else if (sort_by) {
      url =sortUrl;
   }
   else if (sort_by) {
	url =rvspUrl;
 }     
		if(sort_by||rvspFilter){
			fetch('http://api.societree.me/api/events?q={'+url+'}')
      
			.then(results => {
        
			  return results.json();
			})
      
      .then(data=> this.setState({
        events:data.objects
        }))
      
    }
		else this.componentDidMount();
    
		  }
     
	render() {
   
	const event=
              
     this.state.pageOfItems.map((item,index) =>
                      <Link to ={'/events/'+item.event_id} key ={item.event_id} >
                      <Col xs={6} md={4}>
                        <Thumbnail rounded className = 'event' src="https://images.unsplash.com/photo-1513898827383-235b22da454c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=7973f0e8bebad429bb589f60f82a7d81&auto=format&fit=crop&w=800&q=60" alt="242x200">
                          <h5 id='event'>{item.name}</h5>
                         
                         </Thumbnail>
                      </Col>
                      </Link>
                    );
        
   
  

				
		const SelectPackage = require('react-select');
		const Select = SelectPackage.default;
	    const {selectedRvsp} = this.state;
		const {selectedSort} = this.state;
        const rvspValue = selectedRvsp && selectedRvsp.value;
		const sortValue = selectedSort && selectedSort.value

		return (
	<div>
        	<h1 className= "text-center "style={{color:'blue'}}> Events </h1>

		<div className="text-center">
		<h3><Search/></h3>
		<div style={{padding:'10px'}}className="row text-center">

						
						
            
						
						<div className="filter col">
							<h3 style={{color:"white"}}>Sort By
							<Select
									id="sort"
									name="form-field-name"
									value={sortValue}
									onChange={this.handleSortChange.bind(this)}
									options={[
										{value: 'asc', label: 'Name: A - Z'},
										{value: 'desc', label: 'Name: Z - A'},
										
										
									]}
							/>
							</h3>
						</div>			
						<div className="filter col">
						<h3 style={{color:"white"}}>Filter RVSP
							<Select
									id="invite"
									name="form-field-name"
									value={rvspValue}
									onChange={this.handleRvspChange.bind(this)}
									options={[
										{value: '5', label: '+5'},
										{value: '10', label: '+10'},
										{value: '15', label: '+15'},
										
										
										
									]}
							/>
						</h3>
						</div>
					</div>
					
					</div>
					{event}
					<Pagination items={this.state.events} onChangePage={this.onChangePage.bind(this)} />
	</div>
	
			);
	}
}

export default Locations;