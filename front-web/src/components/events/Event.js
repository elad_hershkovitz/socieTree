import React from 'react';
import './event.css'
import {Link}from 'react-router-dom'
import {Thumbnail , Button}from 'react-bootstrap'
export default class event extends React.Component{
  
state = {
    event:[]
  

}

componentDidMount() {
  fetch("http://api.societree.me/api/events/"+this.props.match.params.id)
  .then(results => {
    return results.json();
  }).then(data=> this.setState({
    event:data
    
  }));
}


render(){
   
return ( <div className ="item_event">
<Thumbnail id='event'src="https://images.unsplash.com/photo-1513898827383-235b22da454c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=7973f0e8bebad429bb589f60f82a7d81&auto=format&fit=crop&w=800&q=60" alt="242x200">
          
         <h3>{this.state.event.name} </h3>
          <p>Start at  :{this.state.event.start} </p>
          <p>Ends  :{this.state.event.end} </p>
          <p> info : {this.state.event.description}</p>
          <Button  id='bb' href={this.state.event.url} color="danger">Event page</Button>
          <Link to ="/events">
          <Button id='bb2'color="danger">GO Back</Button>
         </Link>
         </Thumbnail> 
         </div>        
);
}
}
