import { Route, NavLink,  HashRouter,Switch } from "react-router-dom";
  import React from "react";
  import Home from "./home";
  import Stores from "./stores/stores";
  import Events from "./events/events";
  import Trees from './trees/trees';
  import Parks from "./parks/parks";
  import About from "./about/about";
  import Tree from './trees/Tree';
  import event from './events/Event';
  import Park from './parks/Park';
  import Store from './stores/Store';
  import Search from './Search';
  import "./Main.css"
  import Icon  from'./icon.svg'

export default class Main extends React.Component {
    render() {
      return (
        <HashRouter>
          <div>
            
            <ul className="header">
            <h1>
              <img style={{maxBlockSize:"100px"}}src={Icon}/>
              <li id = "welcome"> Welcome to SocieTree</li>
              <li><NavLink exact to="/" style={{color:'white'}}  activeClassName='wow'  activeStyle={{color:'red'}}>Home</NavLink></li>
              <li><NavLink exact to="/trees" style={{color:'white'}}  activeClassName='wow'  activeStyle={{color:'green'}}>Trees</NavLink></li>
              <li><NavLink exact to="/events" style={{color:'white'}}  activeClassName='wow'  activeStyle={{color:'blue'}}>Events</NavLink></li>
              <li><NavLink exact to="/parks" style={{color:'white'}}  activeClassName='wow'  activeStyle={{color:'yellow'}}>Parks</NavLink></li>
              <li><NavLink exact to="/stores" style={{color:'white'}}  activeClassName='wow'  activeStyle={{color:'brown'}}>Stores</NavLink></li>
              <li><NavLink exact to="/about" style={{color:'white'}}  activeClassName='wow'  activeStyle={{color:'gold'}}>About</NavLink></li>
            <li><NavLink exact to="/search" style={{color:'white'}}  activeClassName='wow'  activeStyle={{color:'pink'}}>Search</NavLink></li>
            </h1>
            </ul>     
            <div className="content">
               <Switch>           
              <Route exact path="/" component={Home}/>
              <Route exact path="/trees" component={Trees}/>
              <Route exact path="/events" component={Events}/>
              <Route exact path="/parks" component={Parks}/>
              <Route exact path="/stores" component={Stores}/>
              <Route exact path="/about" component={About}/>
              <Route exact path="/stores/:id" component={Store}/>
              <Route exact path="/trees/:id" component={Tree}/>
              <Route exact path="/parks/:id" component={Park}/>
              <Route exact path="/events/:id" component={event}/>
              <Route path='/search' component={Search} />
              </Switch>
            </div>
          </div>
        </HashRouter>
      );
    }
  }
