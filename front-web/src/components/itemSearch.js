
import React, {Component} from 'react';
import {Button,Col,Thumbnail} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import {SearchResults} from './SearchResults';
import Highlighter from "react-highlight-words";
import styles from './highlighter.css';

class Search extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pageOfItems: [ ],
      searchTrem:this.props.searchTrem,
      trees: [ ],
      events:[ ],
      stores:[ ],
      parks:[ ],
      results:[ ],
      term: '',
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

   handleChange(event) {
    this.setState({value: event.target.value});
  }

  handleSubmit(event) {
   let termSearch = this.props.searchTerm;
   let url = this.props.url+ encodeURI(this.state.value) + '&json=1';
     fetch(url)
   .then(results => {
     return results.json();
     }).then(
      
      
      data=> this.setState({
      trees:data.trees,
      events:data.events,
      parks:data.parks,
      stores:data.stores,
      
       }));
      
 }

  onChangePage(pageOfItems) {
		// update state with new page of items
    
		this.setState({ pageOfItems: pageOfItems });
	};	  
  highlightText(text) {
		return (
			<Highlighter
				 highlightClassName={ {color:'black'}}
				 highlightStyle={{ color: 'black' }}
				 searchWords = {this.state.value.split(" ")}
				 autoEscape={false}
				 textToHighlight= {text}
			/>
		)
}


  render() {

                const	 tree  = 
                
              this.state.trees.map((item,index) =>
                                
                                <Link to ={'/trees/'+item.tree_id} key ={item.tree_id} >
                                  <li xs={6} md={4}>
                                    
                                      <h5 style={{color:'white'}}>{this.highlightText(item.name)}</h5>
                                  
                                        
                                  {(item.scientific_name).toUpperCase().includes(this.state.value.toUpperCase())&&<h5 > scientific name:  {this.highlightText(item.scientific_name)}</h5>}      
                                {(item.attributes).toUpperCase().includes(this.state.value.toUpperCase())&&<h5>attributes : {this.highlightText(item.attributes)}</h5>}
                                {(item.features).toUpperCase().includes(this.state.value.toUpperCase())&&<h5>features :{this.highlightText(item.features)}</h5>}
                                  {(item.growth_rate).toUpperCase().includes(this.state.value.toUpperCase())&& <h5 > growth rate :{this.highlightText(item.growth_rate)}</h5>}   
                                  {(item.problems).toUpperCase().includes(this.state.value.toUpperCase())&&  <h5 > problems {this.highlightText(item.problems)}</h5>}
                                  </li>
                                  </Link>
                              );

                const	 event  =        
                this.state.events.map((item,index) =>
                                  <Link to ={'/events/'+item.event_id} key ={item.event_id} >
                                   <li xs={6} md={4}>
                                      
                                        <h5 style={{color:'white'}} >{this.highlightText(item.name)}</h5>
                                         {(item.description).toUpperCase().includes(this.state.value.toUpperCase())&&<h5 > description:  {this.highlightText(item.description)}</h5>}      
                                {/* {(item.end).toUpperCase().includes(this.state.value.toUpperCase())&&<h5>end time : {this.highlightText(item.end)}</h5>}
                                  {(item.start).toUpperCase().includes(this.state.value.toUpperCase())&&<h5>start time :{this.highlightText(item.start)}</h5>}
                                 */}
               
                                    </li>
                                    </Link>
                               );
               const	 store  =        
                this.state.stores.map((item,index) =>
                                  <Link to ={'/stores/'+item.store_id} key ={item.event_id} >
                                   <li xs={6} md={4}>
                                      
                                        <h5 style={{color:'white'}}>{this.highlightText(item.name)}</h5>
                                  {/*      
                                            {(item.description).toUpperCase().includes(this.state.value.toUpperCase())&&<h5 > description:  {this.highlightText(item.description)}</h5>}      
                                  {(item.end).toUpperCase().includes(this.state.value.toUpperCase())&&<h5>end time : {this.highlightText(item.end)}</h5>}
                                  {(item.start).toUpperCase().includes(this.state.value.toUpperCase())&&<h5>start time :{this.highlightText(item.start)}</h5>}
                                 */}
               
                                    </li>
                                    </Link>
                               );
               const	 park  = 
                      
                this.state.parks.map((item,index) =>
                                  <Link to ={'/parks/'+item.park_id} key ={item.event_id} >
                                   <li xs={6} md={4}>
                                      
                                        <h5 style={{color:'white'}}> {this.highlightText(item.name)}</h5>
                              {/*           
                                          {(item.address).toUpperCase().includes(this.state.value.toUpperCase())&&<h5 > address:  {this.highlightText(item.address)}</h5>}      
                                  {(item.rating).toUpperCase().includes(this.state.value.toUpperCase())&&<h5>end time : {this.highlightText(item.rating)}</h5>}
                                */} 
                                 
               
                                    </li>
                                    </Link>
                               );

    return (
      <div>
         <form  className='text-center' onSubmit={this.handleSubmit}>
        <label>
        <input  type="text" value={this.state.value} onChange={this.handleChange} />
        </label>
        <input type="submit" value="Search" />
      </form>

  
      
      {this.state.trees.length > 0 
     && <h1  style={{color:'white'}} >Trees results</h1>
      			
      } {tree}    
       
    </div>
    
  );
  }
}

export default Search;

