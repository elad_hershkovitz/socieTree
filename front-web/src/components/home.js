import React from 'react';
import {Carousel,Jumbotron} from 'react-bootstrap';
import './home.css';
export default class Home extends React.Component{
render(){
    return (
        <div>
            
            
            <Carousel>
  <Carousel.Item>
    <img class="img-responsive center-block" width={1600} height={500} alt="90x5000" src="https://images.unsplash.com/photo-1483937781853-2907c01dc56d?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=eebd0b46a1638d31077d43f93d68bcad&auto=format&fit=crop&w=1000&q=60" />
    <Carousel.Caption>
      <h3 id = 'h3-home'>Trees</h3>
      <p id ='home'>Learn about Tress that You Love</p>
    </Carousel.Caption>
  </Carousel.Item>
  <Carousel.Item>
    <img class="img-responsive center-block" width={1600} height={500} alt="900x500" src="https://images.unsplash.com/photo-1502208876445-42a7af8b39e9?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=5f2aa224187bb5eb99e53874d03defeb&auto=format&fit=crop&w=900&q=60" />
    <Carousel.Caption>
      <h3 id = 'h3-home'>Events</h3>
      <p id ='home'>Connect to Other Trees Lovers</p>
    </Carousel.Caption>
  </Carousel.Item>
  <Carousel.Item>
    <img class="img-responsive center-block"  width={1600} height={500} alt="900x500" src="https://images.unsplash.com/photo-1487070183336-b863922373d4?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=6d34026de28fc574bcd9fb3d230be17f&auto=format&fit=crop&w=900&q=60" />
    <Carousel.Caption>
      <h3 id = 'h3-home'>Stores</h3>
      <p id ='home'>Find the right place to shop</p>
    </Carousel.Caption>
  </Carousel.Item>
</Carousel>
        </div>


    );
}


}