import React, { Component } from 'react';
import { Link,} from 'react-router-dom';
import 'react-select/dist/react-select.css';
import Pagination from '../pagination';
import {Thumbnail,Col} from 'react-bootstrap';
import Search from './storeSearch';
import './stores.css'
class Locations extends Component {

	constructor () {
		super();
		this.state = {
			stores:[],
			locations: [],
			pageOfItems: [],
			navigate: false,
			navigateTo: '',
			navigateTo: "",
			stores_list: [],
			selectedRate: {
				value: undefined,
				label: undefined
			},
      
			selectedSort: {
				value: undefined,
				label: undefined,
			},
			
		};
		this.returnNoResults = this.returnNoResults.bind(this);
		this.onChangePage = this.onChangePage.bind(this);
	};

	// Initial load of data into page
	componentDidMount() {
		fetch("http://api.societree.me/api/stores")
		.then(results => {
		  return results.json();
		}).then(data=> this.setState({
		  stores:data.objects,
		}))
		
	  }

	  onChangePage(pageOfItems) {
		// update state with new page of items
    
		this.setState({ pageOfItems: pageOfItems });
	};	  

	
	handleRateChange(selectedRate){
		if (selectedRate == null) {
			this.state.selectedRate = {
				value: undefined,
				label: undefined
			};
		} else if (selectedRate){
			this.state.selectedRate = selectedRate;
			this.setState({selectedRate: selectedRate});
		}
		this.update();
	}

	// Sets state to selected method to sort by
	handleSortChange(selectedSort) {
		if (selectedSort == null) {
			this.state.selectedSort = {
				value: undefined,
				label: undefined
		}
		} else if (selectedSort){
		
			this.state.selectedSort = selectedSort;
			this.setState({ selectedSort:selectedSort });
		}
		this.update();
	}

	// Set state to the selected size cutoff
	handleSizeChange (selectedSize){

		if (selectedSize == null) {
			this.state.selectedSize = {
				value: undefined,
				label: undefined
			};
		} else if (selectedSize) {
		
			this.setState({selected: selectedSize});
		}
		this.update();
	}

	// If no data is return from fetch call, print No Results message
	returnNoResults() {
			return (
				<div className="intro-text text-center bg-faded p-5 rounded">
						<span className="section-heading-upper text-center">No Results</span>
				</div>
			)
		}

	update () {
	
		
		let sort_by = this.state.selectedSort.value;
   	let rateFilter= this.state.selectedRate.value;
    let rateUrl ='"filters":[{"name":"rating","op":"ge","val":"'+rateFilter+'"}]';
    let sortUrl='"order_by":[{"field":"name","direction":"'+sort_by+'"}]';
    let url = rateUrl;
   if(sort_by && rateFilter){
       url =rateUrl+','+sortUrl;
   }
   else if (sort_by) {
      url =sortUrl;
   }   
		if(sort_by||rateFilter){
			fetch('http://api.societree.me/api/stores?q={'+url+'}')
      
			.then(results => {
        
			  return results.json();
			})
      
      .then(data=> this.setState({
            stores:data.objects
      		}))
      
    }
		else this.componentDidMount();
    
		  }
     
	render() {
   
	const	 store=
              
     this.state.pageOfItems.map((item,index) =>
                      <Link key={index}to ={'/stores/'+item.store_id} key ={item.store_id} >
                      <Col xs={6} md={4}>
                        <Thumbnail rounded>
						<img id='storeimg' src={"https://maps.googleapis.com/maps/api/place/photo?maxwidth=5000&photoreference="+item.photo_reference +"&key=AIzaSyAKiUXcyeqYZsgTieBEdrDjcr2qsHT06BU"} alt="242x200"/>
                          <h5 id='store'>{item.name}</h5>
                         
                         </Thumbnail>
                      </Col>
                      </Link>
                    );
        
   
  			
		const SelectPackage = require('react-select');
		const Select = SelectPackage.default;
		const {selectedRate} = this.state;
		const {selectedSort} = this.state;
    

		
  	    const rating = selectedRate && selectedRate.value;
		const sortValue = selectedSort && selectedSort.value

		return (
	<div>

  	<h1 className= "text-center "style={{color:'red'}}> Stores </h1>
	 
		<div className=" text-center">
		

			<h3>
			<Search />
			</h3>			
			<div style={{padding:'10px'}}className="row text-center">
			<div className="filter col text-center">
                  <h1 style={{color:"white"}}>Filter Rating 
              <Select
									id="growthfilter"
									name="form-field-name"
                  					value={rating}
									onChange={this.handleRateChange.bind(this)}
									options={[
										
									
										 
                    {value: '4.2', label: '+4.2'},
                    {value: '4.5', label: '+4.5'},
					{value: '4.6', label: '+4.6'},
                     {value: '4.8', label: '+4.8'},
                      {value: '4.99', label: '5'}
									]}
							/>
				 </h1>
				</div>	
						<div className="filter col">
							<h1 style={{color:"white"}}>Sort By
							<Select
									id="sort"
									name="form-field-name"
									value={sortValue}
									onChange={this.handleSortChange.bind(this)}
									options={[
										{value: 'asc', label: 'Name: A - Z'},
										{value: 'desc', label: 'Name: Z - A'},
										
										
									]}
							/>
					</h1>
						</div>
					</div>
					</div>
					{store}
					
				
					<Pagination items={this.state.stores} onChangePage={this.onChangePage.bind(this)} />
				</div>
			);
	}
}

export default Locations;