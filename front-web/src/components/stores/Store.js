import React from 'react';
import './Store.css'
import {Link}from 'react-router-dom'
import {Thumbnail ,Image ,Button,}from 'react-bootstrap'


export default class Store extends React.Component{
  
state = {
    store:[],
    trees:[],
    

}

componentDidMount() {
  fetch("http://api.societree.me/api/stores/"+this.props.match.params.id)
  .then(results => {
    return results.json();
  }).then(data=> this.setState({
    store:data,
   
  }));
  
}


render(){
  
return ( <div className ="text-center">



<Thumbnail className='text-center' >
          <Image  style={{width: '50%'}} responsive circle src={"https://maps.googleapis.com/maps/api/place/photo?maxwidth=5000&photoreference="+this.state.store.photo_reference +"&key=AIzaSyAKiUXcyeqYZsgTieBEdrDjcr2qsHT06BU"} alt="242x200"/>
          
           <h3>{this.state.store.name} </h3>
          <h3>Address :{this.state.store.address} </h3>
          <h3>Hours :{this.state.store.hours} </h3>
          <h3>Rating :{this.state.store.rating} </h3>
         
          <Link to ="/stores">
          <Button color="danger">GO Back</Button>
         </Link>
         </Thumbnail> 
         <div>trees selections : 
           <Link to='./trees/'
           
            />

         </div>
         </div>        
);
}
}
