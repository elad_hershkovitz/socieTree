import React from 'react';
import {Route,NavLink,HashRouter} from 'react-router-dom'
import {Nav,Navbar,NavItem} from 'react-bootstrap';
export default class MyNavbar extends React.Component{
render(){
    return (
        <div>
       <Navbar inverse collapseOnSelect>
  <Navbar.Header>
    <Navbar.Brand>
      <a href="/">SocieTree</a>
    </Navbar.Brand>
    <Navbar.Toggle />
  </Navbar.Header>
  <Navbar.Collapse>
    <Nav>
      <NavItem eventKey={1} href="/home">
        Home
      </NavItem>
      <NavItem eventKey={2} href="/trees">
        Trees
      </NavItem>
      <NavItem eventKey={3} href="/stores">
        Stores
      </NavItem>
      <NavItem eventKey={4} href="/locations">
        Locations
      </NavItem>
      <NavItem eventKey={5} href="/events">
        Events
      </NavItem>
    </Nav>
    <Nav pullRight>
      <NavItem eventKey={1} href="/about">
        About
      </NavItem>
    </Nav>
 </Navbar.Collapse>
</Navbar>
        </div>


    );
}


}
